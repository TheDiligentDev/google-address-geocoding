import Vue from 'vue'
import App from './App.vue'

//bootstrap vue
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

//Google Maps
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'YOUR_GOOGLE_API_KEY',
  },
})

//firebase
import firebase from 'firebase';
//Paste in your firebase config we copied from the last step
var firebaseConfig = {  
  apiKey: '<your-api-key>',
  authDomain: '<your-auth-domain>',
  databaseURL: '<your-database-url>',
  projectId: '<your-cloud-firestore-project>',
  storageBucket: '<your-storage-bucket>',
  messagingSenderId: '<your-sender-id>',
  appId: '<your-app-id>',
};

firebase.initializeApp(firebaseConfig);
const Firestore = firebase.firestore;
export const db = Firestore();

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')